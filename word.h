//
// Created by User on 02.11.2021..
//

#ifndef AES_WORD_H
#define AES_WORD_H

#include "byte.h"

class Word {

public:
    //Properties
    Byte* bytes[4]{};

    //Constructors
    Word();
    explicit Word(const unsigned char *data);

    //Transformations

    //Moves bytes to the left (right if bool set to false)
    Word* circularShift(bool left = true);   //Used in ShiftRows and Key Expansion G function
    Word* subBytes(bool inverse = false);

    //Operators

    //Compare if two words have the same bytes
    friend bool operator==(const Word &word_a, const Word &word_b);
    friend bool operator!=(const Word &word_a, const Word &word_b);
    //Print out value in dec, hex and bin
    friend ostream &operator<<(ostream &os, Word word);
    //Apply exclusive or to each byte and return the resulting word
    friend Word operator xor(const Word &word_a, const Word &word_b);
};


#endif //AES_WORD_H
