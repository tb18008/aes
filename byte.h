//
// Created by User on 01.11.2021..
//

#ifndef AES_BYTE_H
#define AES_BYTE_H

#include <sstream>

using namespace std;

class Byte {
    private:
        bool bits[8] = {};
        unsigned char value = 0;

    public:

        explicit Byte(unsigned char value = 0);

        string printBinary();
        void substituteByte(bool inverse = false);
        void shiftDiscard(bool left = true);

        //Clear all bits and set them according to new value
        void setValue(unsigned char new_value);
        void setValue();    //Calculate new value from bits
        int getValue() const;

        //Compare if two bytes have the same value and bits
        friend bool operator==(const Byte& byte_a,const Byte& byte_b);
        friend bool operator!=(const Byte& byte_a,const Byte& byte_b);

        //Print out value in dec, hex and bin
        friend ostream& operator<<(ostream& os, Byte byte);

        //Return the exclusive or result of two bytes
        friend Byte operator xor(Byte byte_a, Byte byte_b);
        friend Byte operator xor(Byte* byte_a, Byte byte_b);
        friend Byte operator xor(Byte byte_a, Byte* byte_b);

        //Multiplication in Rijndael's finite field
        friend Byte operator *(Byte byte_a, Byte byte_b);
        friend Byte operator *(Byte* byte_a, Byte byte_b);
        friend Byte operator *(Byte byte_a, Byte* byte_b);
};


#endif //AES_BYTE_H
