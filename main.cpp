#include <iostream>
#include "byte.h"
#include "word.h"
#include "aes.h"

using namespace std;

void testByte(){
    Byte byte;
    for (int i = 0; i < 256; ++i) {
        byte = Byte(i);

        //Print if byte and i doesn't match
        if(i!=byte.getValue()) {
            cout << "\t[ERROR] Byte Test:\n";
            cout << "Byte value:\n"<<byte.getValue()<<endl;
            cout << "Expected byte value:\n"<<i<<endl;
            return;
        }
    }
    cout << "\t[OK] Byte\n";
}
void testWord(){

    unsigned char int_data[4] = {1, 2, 3, 4};

    Word word = Word(int_data);

    for (int i = 0; i < 4; ++i) {
        //Print if word bytes and array elements dont match
        if(int_data[i]!=word.bytes[i]->getValue()) {
            cout << "\t[ERROR] Word Test:\n";
            cout << "Word byte value:\n"<<word.bytes[i]->getValue()<<endl;
            cout << "Expected word byte value:\n"<<int_data[i]<<endl;
            return;
        }
    }
    cout << "\t[OK] Word\n";
}
void testWordCircularShift(){

    Word word_before;
    Word word_after;
    Word expected_word;

    unsigned char int_data[4] = {1, 2, 3, 4};
    unsigned char expected_byte_values[4] = {};

    //Shift left test
    word_before = Word(int_data);
    word_after = word_before;

    expected_byte_values[0] = 2;
    expected_byte_values[1] = 3;
    expected_byte_values[2] = 4;
    expected_byte_values[3] = 1;

    expected_word = Word(expected_byte_values);

    word_after.circularShift();

    for (int i = 0; i < 4; ++i) {
        //Print if word_before bytes and array elements dont match
        if(expected_word != word_after) {
            cout << "\t[ERROR] Word Circular Shift Test (Shift Left):\n";
            cout << "Word before shift:\n" << word_before << endl;
            cout << "Word after shift:\n" << word_after << endl;
            cout << "Expected word:\n"<<expected_word<<endl;
            return;
        }
    }
    //Shift right test
    word_before = Word(int_data);
    word_after = word_before;

    expected_byte_values[0] = 4;
    expected_byte_values[1] = 1;
    expected_byte_values[2] = 2;
    expected_byte_values[3] = 3;

    expected_word = Word(expected_byte_values);

    word_after.circularShift(false);

    for (int i = 0; i < 4; ++i) {
        //Print if word_before bytes and array elements dont match
        if(expected_word != word_after) {
            cout << "\t[ERROR] Word Circular Shift Test (Shift Right):\n";
            cout << "Word before shift:\n" << word_before << endl;
            cout << "Word after shift:\n" << word_after << endl;
            cout << "Expected word:\n"<<expected_word<<endl;
            return;
        }
    }
    cout << "\t[OK] Word Circular Shift\n";
}
void testByteSubstitution(){

    //Array to store
    unsigned char comparison_array[256];
    Byte substituted_byte_before;
    Byte substituted_byte_after;
    Byte expected_byte;

    //Iterate through each test sample
    for (int i = 0; i < 256; ++i) {

        substituted_byte_before = Byte(i);
        substituted_byte_after = Byte(i);
        substituted_byte_after.substituteByte();

        expected_byte = Byte((int)aes_byte_sbox[i]);

        //Print if after substitution the value doesn't match
        if(substituted_byte_after != expected_byte) {
            cout << "\t[ERROR] Byte Substitution Test:\n";
            cout << "Substituted byte before:\n"<<substituted_byte_before<<endl;
            cout << "Substituted byte after:\n"<<substituted_byte_after<<endl;
            cout << "Expected byte:\n"<<expected_byte<<endl;
            return;
        }
    }
    cout << "\t[OK] Byte Substitution\n";
}
void testByteXOR(){
    Byte byte;
    Byte operand;

    unsigned int byte_val_before_xor = 255;
    unsigned int operands[4] = {0,1,170,255};
    unsigned int byte_val_after_xor[4] = {255,254,85,0};

    //Iterate through each test sample
    for (int i = 0; i < 4; ++i) {

        byte = Byte(byte_val_before_xor);
        operand = Byte(operands[i]);
        byte = byte xor operand;    //Do XOR operation

        if(byte.getValue() != byte_val_after_xor[i]){
            cout << "\t[ERROR] Byte XOR Test:\n";
            cout << "Byte value before:\n"<<byte_val_before_xor<<endl;
            cout << "Operand value:\n"<<operand.getValue()<<endl;
            cout << "Byte value after:\n"<<byte.getValue()<<endl;
            cout << "Expected byte value:\n"<<byte_val_after_xor[i]<<endl;
            return;
        }
    }
    cout << "\t[OK] Byte XOR\n";
}
void testWordXOR(){
    Word word;
    Word operand;

    unsigned char word_bytes_before_xor[4] = {255,255,255,255};
    unsigned char operands[4][4] = {{0,0,0,0},{1,1,1,1},{170,170,170,170},{255,255,255,255}};
    unsigned int byte_val_after_xor[4][4] = {{255,255,255,255},{254,254,254,254},{85,85,85,85},{0,0,0,0}};

    //Iterate through each test sample
    for (int i = 0; i < 4; ++i) {

        word = Word(word_bytes_before_xor);
        operand = Word(operands[i]);
        word = word xor operand;    //Do XOR operation

        //Compare each byte of each test sample
        for (int j = 0; j < 4; ++j) {
            if(word.bytes[j]->getValue() != byte_val_after_xor[i][j]){
                cout << "\t[ERROR] Word XOR Test (test sample = "<<i<<"; word byte = "<<j<<"):\n";
                cout << "Word byte value before:\n"<<(int)word_bytes_before_xor[j]<<endl;
                cout << "Operand word byte value:\n"<<operand.bytes[j]->getValue()<<endl;
                cout << "Word byte value after:\n"<<word.bytes[j]->getValue()<<endl;
                cout << "Expected byte value:\n"<<(int)byte_val_after_xor[i][j]<<endl;
                return;
            }
        }
    }
    cout << "\t[OK] Word XOR\n";
}

/* Gets the key for nth round using the key from (n-1)th round
 *
 * If round equals to 0, then returns back the same key
 *
 * Otherwise uses function G to calculate first word and then the following rows
 * in new key
 *
 * Test sample:
 * https://www.brainkart.com/article/AES-Key-Expansion_8410/
 */
Block getRoundKey(Block old_key, unsigned int round_number){

    Block new_key = old_key;

    //If it's the 0th round then leave the old key as new key
    if(round_number == 0) return new_key;


    //Otherwise calculate new key from old key

    Word round_constant;
    unsigned char round_constants[10][4] = {
            {1, 0, 0, 0},  //1st round
            {2, 0, 0, 0},  //2nd round
            {4, 0, 0, 0},  //3rd round
            {8, 0, 0, 0},  //4th round
            {16, 0, 0, 0},  //5th round
            {32, 0, 0, 0},  //6th round
            {64, 0, 0, 0},  //7th round
            {128, 0, 0, 0},  //8th round
            {27, 0, 0, 0},  //9th round
            {54, 0, 0, 0},  //10th round
    };
    round_constant = Word(round_constants[round_number-1]);

    //First word in new key

    //Get word in same position of old key and xor with G(previous word)
    //Function G: Get last word of old key, circular shift, substitute and xor with round constant
    new_key.rows[0] =
            old_key.rows[0] xor
            //Function G
            *old_key.rows[3].circularShift()->subBytes() xor round_constant;

    //The other three rows in new key

    //Get word in same position of old key and xor with previous word
    for (int i = 1; i < 4; ++i) {
        new_key.rows[i] = old_key.rows[i] xor new_key.rows[i - 1];
    }

    return new_key;
}
void testKeyExpansion(){
    unsigned char eight_round_key_bytes[16] = {
            0xea, 0xd2, 0x73, 0x21, 0xb5, 0x8d, 0xba, 0xd2, 0x31, 0x2b, 0xf5, 0x60, 0x7f, 0x8d, 0x29, 0x2f
    };
    unsigned char expected_word_bytes[4] = {
            0xac, 0x77, 0x66, 0xf3
    };
    Block eight_round_key = Block(eight_round_key_bytes);
    Word expected_word = Word(expected_word_bytes);
    Block ninth_round_key = getRoundKey(eight_round_key, 9);


    if(ninth_round_key.rows[0] != expected_word){
        cout << "\t[ERROR] Key Expansion Round Test:\n";
        cout << "Eight round key:\n"<<eight_round_key<<endl;
        cout << "Ninth round key first word:\n" << ninth_round_key.rows[0] << endl;
        cout << "Expected word:\n"<<expected_word<<endl;
        return;
    }
    cout << "\t[OK] Key Expansion Round\n";
}
void testBlockXOR(){
    unsigned char block_bytes_before[16] = {
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
    };
    unsigned char operand_block_bytes[4][16] = {
            { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
            { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 },
            { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa },
            { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
    };
    unsigned char expected_block_bytes[4][16] = {
            { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff },
            { 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe },
            { 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55 },
            { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
    };
    Block block_before = Block(block_bytes_before);
    Block operand;
    Block block_after;
    Block expected;

    //Iterate through each test sample and compare
    for (int i = 0; i < 4; ++i) {
        operand = Block(operand_block_bytes[i]);
        block_after = block_before xor operand;
        expected = Block(expected_block_bytes[i]);
        if(block_after!=expected){
            cout << "\t[ERROR] Block XOR Test (test sample = "<<i<<"):\n";
            cout << "Block before:\n"<<block_before<<endl;
            cout << "Operand Block:\n"<<operand<<endl;
            cout << "Block after:\n"<<block_after<<endl;
            cout << "Expected Block:\n"<<expected<<endl;
            return;
        }
    }
    cout << "\t[OK] Block XOR\n";
}
void testBlockCircularShift(){
    unsigned char block_bytes_before[16] = {
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
    };
    unsigned char expected_block_bytes[2][16] = {
            {
                0x00, 0x01, 0x02, 0x03,
                0x05, 0x06, 0x07, 0x04,
                0x0a, 0x0b, 0x08, 0x09,
                0x0f, 0x0c, 0x0d, 0x0e
            },
            {
                0x00, 0x01, 0x02, 0x03,
                0x07, 0x04, 0x05, 0x06,
                0x0a, 0x0b, 0x08, 0x09,
                0x0d, 0x0e, 0x0f, 0x0c
            },
    };
    Block block_before = Block(block_bytes_before);
    Block block_after;
    Block expected;

    //Shift rows
    //Iterate through each test sample and compare
    block_after = block_before;
    block_after.shiftRows();
    expected = Block(expected_block_bytes[0]);
    if(block_after!=expected){
        cout << "\t[ERROR] Shift Rows Test (not inverse):\n";
        cout << "Block before:"<<block_before<<endl;
        cout << "Block after:"<<block_after<<endl;
        cout << "Expected Block:"<<expected<<endl;
        return;
    }
    //Shift rows inverse
    block_after = block_before;
    block_after.shiftRows(true);
    expected = Block(expected_block_bytes[1]);
    if(block_after!=expected){
        cout << "\t[ERROR] Block Circular Shift Test (inverse):\n";
        cout << "Block before:"<<block_before<<endl;
        cout << "Block after:"<<block_after<<endl;
        cout << "Expected Block:"<<expected<<endl;
        return;
    }
    cout << "\t[OK] Shift Rows\n";
}
void testSubstituteBytes(){
    unsigned char block_bytes_before[16] = {
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
    };
    unsigned char expected_block_bytes[2][16] = {
            {
                0x00, 0x01, 0x02, 0x03,
                0x05, 0x06, 0x07, 0x04,
                0x0a, 0x0b, 0x08, 0x09,
                0x0f, 0x0c, 0x0d, 0x0e
            },
            {
                0x00, 0x01, 0x02, 0x03,
                0x07, 0x04, 0x05, 0x06,
                0x0a, 0x0b, 0x08, 0x09,
                0x0d, 0x0e, 0x0f, 0x0c
            },
    };
    Block block_before = Block(block_bytes_before);
    Block block_after;
    Block expected_block;

    //Shift rows
    //Iterate through each test sample and compare
    block_after = block_before;
    block_after.shiftRows();
    expected_block = Block(expected_block_bytes[0]);
    if(block_after != expected_block){
        cout << "\t[ERROR] Block Circular Shift Test (not inverse):\n";
        cout << "Block before:\n"<<block_before<<endl;
        cout << "Block after:\n"<<block_after<<endl;
        cout << "Expected Block:\n" << expected_block << endl;
        return;
    }
    //Shift rows inverse
    block_after = block_before;
    block_after.shiftRows(true);
    expected_block = Block(expected_block_bytes[1]);
    if(block_after != expected_block){
        cout << "\t[ERROR] Shift Rows Test (inverse):\n";
        cout << "Block before:\n"<<block_before<<endl;
        cout << "Block after:\n"<<block_after<<endl;
        cout << "Expected Block:\n" << expected_block << endl;
        return;
    }
    cout << "\t[OK] Block Circular Shift\n";
}
void testBlockSubstituteBytes(){

    unsigned char block_bytes_before[16];
    unsigned char s_box_bytes[16];

    Block block_before;
    Block block_after;
    Block expected_block;

    //Not inverse substitution test
    for (int i = 0; i < 16; ++i) {
        for (int j = 0; j < 16; ++j) {
            //Write numbers from 0 - 16 into an array
            block_bytes_before[j] = i*16+j;
            block_before = Block(block_bytes_before);   //Initialize block with the numbers

            //Write numbers from s-box into an array
            s_box_bytes[j] = aes_byte_sbox[i*16+j];
            expected_block = Block(s_box_bytes);   //Initialize block with the numbers
        }
        block_after = block_before;
        //Apply substitution
        block_after.substituteBytes();

        if(block_after!=expected_block){
            cout << "\t[ERROR] Block Substitution Test (not inverse):\n";
            cout << "Block before:\n"<<block_before<<endl;
            cout << "Block after:\n"<<block_after<<endl;
            cout << "Expected Block:\n"<<expected_block<<endl;
            return;
        }
    }
    //Inverse substitution test
    for (int i = 0; i < 16; ++i) {
        for (int j = 0; j < 16; ++j) {
            //Write numbers from 0 - 16 into an array
            block_bytes_before[j] = i*16+j;
            block_before = Block(block_bytes_before);   //Initialize block with the numbers

            //Write numbers from s-box into an array
            s_box_bytes[j] = inv_aes_byte_sbox[i*16+j];
            expected_block = Block(s_box_bytes);   //Initialize block with the numbers
        }
        block_after = block_before;
        //Apply substitution
        block_after.substituteBytes(true);

        if(block_after!=expected_block){
            cout << "\t[ERROR] Block Substitute Bytes Test (inverse):\n";
            cout << "Block before:\n"<<block_before<<endl;
            cout << "Block after:\n"<<block_after<<endl;
            cout << "Expected Block:\n"<<expected_block<<endl;
            return;
        }
    }
    cout << "\t[OK] Block Substitute Bytes\n";
}
void testByteShiftDiscard(){

    Byte byte_before;
    Byte byte_after;
    Byte expected_byte;

    //Shift left test
    for (int i = 0; i < 256; ++i) {
        byte_before = Byte(i);
        byte_after = byte_before;
        byte_after.shiftDiscard(true);

        expected_byte = Byte((i*2) % 256);

        if(byte_after != expected_byte){
            cout << "\t[ERROR] Byte Shift Discard Test (Shift Left):\n";
            cout << "Byte before shift:\n" << byte_before << endl;
            cout << "Byte after shift:\n" << byte_after << endl;
            cout << "Expected word:\n"<<expected_byte<<endl;
            return;
        }
    }
    //Shift right test
    for (int i = 0; i < 256; ++i) {
        byte_before = Byte(i);
        byte_after = byte_before;
        byte_after.shiftDiscard(false);

        expected_byte = Byte(i/2);

        if(byte_after != expected_byte){
            cout << "\t[ERROR] Byte Shift Discard Test (Shift Right):\n";
            cout << "Byte before shift:\n" << byte_before << endl;
            cout << "Byte after shift:\n" << byte_after << endl;
            cout << "Expected word:\n"<<expected_byte<<endl;
            return;
        }
    }
    cout << "\t[OK] Byte Circular Shift\n";
}
void testByteMultiplication(){
    //Test samples from FIPS-197-AES all examples of multiplication
    unsigned char byte_a_values[12] {
        0x53, 0xca, 0x57, 0x57, 0x03, 0x01, 0x01, 0x02, 0x57, 0x57, 0x57, 0x57
    };
    unsigned char byte_b_values[12] {
        0xca, 0x53, 0x83, 0x13, 0x0e, 0x09, 0x0d, 0x0b, 0x02, 0x04, 0x08, 0x10
    };
    unsigned char expected_values[12] {
        0x01, 0x01, 0xc1, 0xfe, 0x12, 0x09, 0x0d, 0x16, 0xae, 0x47, 0x8e, 0x07
    };

    Byte byte_a, byte_b, result_byte, expected_byte;
    for (int i = 0; i < 12; ++i) {
        byte_a = Byte(byte_a_values[i]);
        byte_b = Byte(byte_b_values[i]);
        result_byte = byte_a * byte_b;
        expected_byte = Byte(expected_values[i]);
        if(byte_a * byte_b != expected_byte){
            cout << "\t[ERROR] Byte Multiplication Test:\n";
            cout << "Byte A:\n" << byte_a << endl;
            cout << "Byte B:\n" << byte_b << endl;
            cout << "A * B:\n" << result_byte << endl;
            cout << "Expected byte:\n"<<expected_byte<<endl;
            return;
        }
    }
    cout << "\t[OK] Byte Multiplication\n";
}

void testBlockMixColumns(){
    /* https://en.wikipedia.org/wiki/Rijndael_MixColumns#Test_vectors_for_MixColumn()
     *  Before		    After
        db 13 53 45	    8e 4d a1 bc
        f2 0a 22 5c	    9f dc 58 9d
        01 01 01 01	    01 01 01 01
        c6 c6 c6 c6	    c6 c6 c6 c6
        d4 d4 d4 d5	    d5 d5 d7 d6
        2d 26 31 4c	    4d 7e bd f8
     */

    //Test samples from FIPS-197-AES all examples of multiplication
    unsigned char block_before_byte_values[6][16] = {
            { 0xdb, 0xdb, 0xdb, 0xdb, 0x13, 0x13, 0x13, 0x13, 0x53, 0x53, 0x53, 0x53, 0x45, 0x45, 0x45, 0x45},
            { 0xf2, 0xf2, 0xf2, 0xf2, 0x0a, 0x0a, 0x0a, 0x0a, 0x22, 0x22, 0x22, 0x22, 0x5c, 0x5c, 0x5c, 0x5c},
            { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01},
            { 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6},
            { 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd4, 0xd5, 0xd5, 0xd5, 0xd5},
            { 0x2d, 0x2d, 0x2d, 0x2d, 0x26, 0x26, 0x26, 0x26, 0x31, 0x31, 0x31, 0x31, 0x4c, 0x4c, 0x4c, 0x4c},
    };
    unsigned char expected_values[6][16] = {
            { 0x8e, 0x8e, 0x8e, 0x8e, 0x4d, 0x4d, 0x4d, 0x4d, 0xa1, 0xa1, 0xa1, 0xa1, 0xbc, 0xbc, 0xbc, 0xbc},
            { 0x9f, 0x9f, 0x9f, 0x9f, 0xdc, 0xdc, 0xdc, 0xdc, 0x58, 0x58, 0x58, 0x58, 0x9d, 0x9d, 0x9d, 0x9d},
            { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01},
            { 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6},
            { 0xd5, 0xd5, 0xd5, 0xd5, 0xd5, 0xd5, 0xd5, 0xd5, 0xd7, 0xd7, 0xd7, 0xd7, 0xd6, 0xd6, 0xd6, 0xd6},
            { 0x4d, 0x4d, 0x4d, 0x4d, 0x7e, 0x7e, 0x7e, 0x7e, 0xbd, 0xbd, 0xbd, 0xbd, 0xf8, 0xf8, 0xf8, 0xf8},
    };

    Block block_before;
    Block block_after;
    Block expected_block;

    //Not inverse
    for (int i = 0; i < 6; ++i) {
        block_before = Block(block_before_byte_values[i]);
        block_after = block_before;
        block_after.mixColumns();

        expected_block = Block(expected_values[i]);

        if(block_after!=expected_block){
            cout << "\t[ERROR] Block Mix Columns Test (not inverse):\n";
            cout << "Block before: " << block_before << endl;
            cout << "Block after: " << block_after << endl;
            cout << "Expected block: "<<expected_block<<endl;
            return;
        }
    }

    //Inverse
    for (int i = 0; i < 6; ++i) {
        block_before = Block(expected_values[i]);
        block_after = block_before;
        block_after.mixColumns(true);

        expected_block = Block(block_before_byte_values[i]);

        if(block_after!=expected_block){
            cout << "\t[ERROR] Block Mix Columns Test (inverse):\n";
            cout << "Block before: " << block_before << endl;
            cout << "Block after: " << block_after << endl;
            cout << "Expected block: "<<expected_block<<endl;
            return;
        }
    }

    cout << "\t[OK] Block Mix Columns\n";
}

void testController() {

    unsigned char plaintext_a[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    unsigned char ciphertext_a[16] = {};
    unsigned char key[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    unsigned char expected_result[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

    AES aes;
    aes.encryptBlock(plaintext_a, key, ciphertext_a);
    for (int i = 0; i < 16; ++i) {
        if(ciphertext_a[i] != expected_result[i]){
            cout << "\t[ERROR] Controller Test (encryption):\n";
            cout << "Plaintext: ";
            for (int j = 0; j < 16; ++j) cout << plaintext_a[j];
            cout << endl;
            cout << "Ciphertext: ";
            for (int j = 0; j < 16; ++j) cout << ciphertext_a[j];
            cout << endl;
            cout << "Expected ciphertext_a: ";
            for (int j = 0; j < 16; ++j) cout << expected_result[j];
            cout << endl;
            return;
        }
    }

    unsigned char ciphertext_b[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    unsigned char plaintext_b[16] = {};

    aes.decryptBlock(ciphertext_b, key, plaintext_b);
    for (int i = 0; i < 16; ++i) {
        if(plaintext_b[i] != expected_result[i]){
            cout << "\t[ERROR] Controller Test (decryption):\n";
            cout << "Ciphertext: ";
            for (int j = 0; j < 16; ++j) cout << ciphertext_b[j];
            cout << endl;
            cout << "Plaintext: ";
            for (int j = 0; j < 16; ++j) cout << plaintext_b[j];
            cout << endl;
            cout << "Expected ciphertext_a: ";
            for (int j = 0; j < 16; ++j) cout << expected_result[j];
            cout << endl;
            return;
        }
    }

    cout << "\t[OK] Controller\n";
}

int main() {

    //Regression tests

    //Byte tests
    testByte();
    testByteShiftDiscard();
    testByteXOR();
    testByteSubstitution();
    testByteMultiplication();
    cout << endl;

    //Word tests
    testWord();
    testWordCircularShift();
    testWordXOR();
    cout << endl;

    testKeyExpansion();

    //Block tests
    testBlockXOR();
    testBlockCircularShift();
    testBlockSubstituteBytes();
    testBlockMixColumns();
    cout << endl;

    //AES controller tests
    testController();

    return 0;
}
