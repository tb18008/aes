//
// Created by User on 02.11.2021..
//

#include "word.h"
#include <iostream>

using namespace std;

Word::Word() {
    for (int i = 0; i < 4; ++i) this->bytes[i] = new Byte(0);
}

Word::Word(const unsigned char *data) {
    for (int i = 0; i < 4; ++i) this->bytes[i] = new Byte(data[i]);
}

Word* Word::circularShift(bool left) {

    Byte* moved_byte;

    //Shifting bytes to the left
    if(left){

        //Save the first byte before it is overwritten
        moved_byte = this->bytes[0];

        //Shift last three bytes to left
        for (int i = 1; i < 4; ++i) this->bytes[i-1] = this->bytes[i];

        this->bytes[3] = moved_byte;
    }
    //Shifting bytes to the right
    else{

        //Save the last byte before it is overwritten
        moved_byte = this->bytes[3];

        //Shift last three bytes to left
        for (int i = 3; i >= 0; --i) this->bytes[i] = this->bytes[i-1];

        this->bytes[0] = moved_byte;
    }

    return this;
}

Word* Word::subBytes(bool inverse) {
    for (auto & byte : this->bytes) byte->substituteByte(inverse);
    return this;
}

ostream& operator<<(ostream& os, Word word){
    unsigned int byte_value;

    os << "Word(";  //Print starting parenthesis

    //Hexadecimal
    byte_value = word.bytes[0]->getValue();
    if(byte_value < 16) os << " 0x0" <<hex<< byte_value;
    else os << " 0x" <<hex<<byte_value;
    for (int i = 1; i < 4; ++i) {
        byte_value = word.bytes[i]->getValue();
        if(byte_value < 16) os << "0" << hex << byte_value;
        else os << hex << byte_value;
    }

    //Binary
    os<<" ["<<dec<<word.bytes[0]->printBinary();
    for (int i = 1; i < 4; ++i) os<<' '<<word.bytes[i]->printBinary();
    os <<"])";  //Print ending parenthesis
    return os;
}

bool operator==(const Word &word_a, const Word &word_b) {
    for (int i = 0; i < 4; ++i) {
        if(word_a.bytes[i]->getValue() != word_b.bytes[i]->getValue()) return false;
    }
    return true;
}

bool operator!=(const Word &word_a, const Word &word_b) {
    return !(word_a==word_b);
}

Word operator xor(const Word &word_a, const Word &word_b) {
    Word new_word = Word();
    for (int i = 0; i < 4; ++i) *new_word.bytes[i] = (*word_a.bytes[i] xor *word_b.bytes[i]);
    return new_word;
}
