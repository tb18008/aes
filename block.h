//
// Created by User on 06.11.2021..
//

#ifndef AES_BLOCK_H
#define AES_BLOCK_H

#include "word.h"

class Block {
    private:
        //Properties
        Byte bytes[16];

    public:
        //Properties
        Word rows[4];   //Used in key expansion

        //Constructors
        Block();
        explicit Block(const unsigned char *bytes);

        //Transformations
        Block* shiftRows(bool inverse = false);  //Circular shift to each word different amount of times
        Block* substituteBytes(bool inverse = false);   //Apply byte substitution to each byte in each word of block
        Block* mixColumns(bool inverse = false);  //Circular shift to each word different amount of times

        //Methods
        void setValue(const unsigned char *key_bytes);
        void circularShift(unsigned char row, bool left = false);
        void mixColumn(unsigned char column, bool inverse = false);
        unsigned char getByte(unsigned char byte_index);

        //Operators

        //Compare if the bits of two blocks are the same in every position
        friend bool operator==(const Block& block_a, const Block& block_b);
        friend bool operator!=(const Block& block_a, const Block& block_b);
        //Print out just hex value
        friend ostream &operator<<(ostream &os, Block* block);
        friend ostream &operator<<(ostream &os, Block block);
        //Applies xor to each word of the two blocks and returns resulting key block
        friend Block operator xor(const Block& block_a, const Block& block_b);
};


#endif //AES_BLOCK_H
