//
// Created by User on 01.11.2021..
//

#ifndef AES_AES_H
#define AES_AES_H

#include "block.h"
#include "sbox.h"

static const unsigned char round_constants[10] = {
        /* 1     2     3     4     5     6     7     8     9    10*/
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36
};

class AES {

    private:
        //Methods
        void writeOutBlock(Block block, unsigned char (&array)[16]);

    public:

        AES();  //Constructor

        //Methods
        void encryptBlock(const unsigned char *plaintext, const unsigned char *key, unsigned char (&ciphertext_result)[16]);
        void decryptBlock(const unsigned char *ciphertext, const unsigned char *key, unsigned char (&plaintext_result)[16]);

};


#endif //AES_AES_H
