//
// Created by User on 01.11.2021..
//


#include "byte.h"
#include <iostream>
#include <cmath>
#include "sbox.h"

using namespace std;

Byte::Byte(unsigned char value) {
    setValue(value);
}

string Byte::printBinary() {
    string result;
    for(int i=7 ;i>=0 ;i--) result += (this->bits[i]) ? '1' : '0';
    return result;
}

void Byte::substituteByte(bool inverse) {
    if(!inverse) this->setValue(aes_byte_sbox[(int)this->value]);
    else this->setValue(inv_aes_byte_sbox[(int)this->value]);
}

bool operator==(const Byte &byte_a, const Byte &byte_b) {
    if(byte_a.value!=byte_b.value) return false;
    for(int i=0; i<8; i++) if(byte_a.bits[i]!=byte_b.bits[i]) return false;
    return true;
}

bool operator!=(const Byte &byte_a, const Byte &byte_b) {
    return !(byte_a==byte_b);
}

ostream &operator<<(ostream &os, Byte byte) {
    unsigned int value = byte.getValue();

    os << "Byte(" << value<< " : 0x";
    if(value < 16) os << "0" << hex << value;
    else os << hex << value;
    os<<dec<< " : ";
    for(int i=7 ;i>=0 ;i--) os<<byte.bits[i];
    os << ')';
    return os;
}

Byte operator xor(Byte byte_a, Byte byte_b) {
    Byte new_byte = Byte();

    for (int i = 0; i < 8; ++i) {
        if(!byte_a.bits[i] && byte_b.bits[i] || byte_a.bits[i] && !byte_b.bits[i]) new_byte.bits[i] = true;
        else  new_byte.bits[i] = false;
    }

    new_byte.setValue();    //Calculate new value from newly set bits

    return new_byte;
}

Byte operator xor(Byte *byte_a, Byte byte_b) {
    return *byte_a xor byte_b;
}
Byte operator xor(Byte byte_a, Byte *byte_b) {
    return byte_a xor *byte_b;
}

Byte operator*(Byte byte_a, Byte byte_b) {
    //https://en.wikipedia.org/wiki/Finite_field_arithmetic#Multiplication

    bool carry;
    Byte irreducible = Byte(0x1b);

    Byte product = Byte(0);
    if(byte_a.getValue() && byte_b.getValue()){ //Run calculation only if both operands are not 0
        for (int i = 0; i < 8; ++i) {
            if(byte_b.getValue() % 2) product = product xor byte_a; //Check if rightmost bit is true
            byte_b.shiftDiscard(false);
            carry = byte_a.getValue() > 127;    //Leftmost bit is true
            byte_a.shiftDiscard(true);
            if(carry) byte_a = byte_a xor irreducible;
        }
    }
    return product;
}

Byte operator*(Byte* byte_a, Byte byte_b) {
    return *byte_a * byte_b;
}
Byte operator*(Byte byte_a, Byte* byte_b) {
    return byte_a * *byte_b;
}

void Byte::setValue(unsigned char new_value) {
    this->value = new_value;
    for (bool & bit : this->bits) bit = false;

    //Set bit boolean values
    unsigned char number = this->value;
    for(int i=0; number>0; i++)
    {
        this->bits[i]=number % 2;
        number = number / 2;
    }
}

void Byte::setValue() {
    //Calculate new value
    unsigned char new_value = 0;
    for (int i = 7; i >= 0; --i) {
        new_value += pow(2, i) * this->bits[i];
    }
    this->value = new_value;
}

int Byte::getValue() const {
    return (int)this->value;
}

void Byte::shiftDiscard(bool left) {

    //Shifting bits to the left
    if(left) this->setValue((this->getValue() * 2) % 256);
    //Shifting bits to the right
    else this->setValue(this->getValue() / 2);
}