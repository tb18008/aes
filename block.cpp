//
// Created by User on 06.11.2021..
//

#include "block.h"
#include <iostream>

Block::Block() {
    unsigned char zero_bytes[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    this->setValue(zero_bytes);
}

Block::Block(const unsigned char *bytes) {
    this->setValue(bytes);
}

void Block::setValue(const unsigned char *key_bytes) {
    /*
     Rows
        Bytes {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, ...
                 |     |     |     |     |     |     |     |     |
       1st row{0x00, 0x01, 0x02, 0x03}   |     |     |     |     |
       2nd row                        {0x05, 0x06, 0x07, 0x08, 0x09}
       ...
    */
    for (int i = 0; i < 16; ++i) {
        this->bytes[i] = Byte(key_bytes[i]);

        this->rows[i/4].bytes[i%4] = &this->bytes[i];
    }
}

ostream &operator<<(ostream &os, Block* block) {
    os << *block;
    return os;
}
ostream &operator<<(ostream &os, Block block) {
    os << "Block(";

    for (int i = 0; i < 16; ++i) {
        if(i % 4 == 0) os << "\n\t";
        if(block.bytes[i].getValue() < 16) os << " 0" << hex << block.bytes[i].getValue();
        else os << ' ' << hex << block.bytes[i].getValue();
    }
    os << "\n)";
    return os;
}

Block operator xor(const Block &block_a, const Block &block_b) {
    Block new_block;
    for (int i = 0; i < 16; ++i) {
        new_block.bytes[i] = block_a.bytes[i] xor block_b.bytes[i];
    }

    return new_block;
}

bool operator==(const Block &block_a, const Block &block_b) {
    for (int i = 0; i < 16; ++i) {
        if(block_a.bytes[i].getValue() != block_b.bytes[i].getValue()) return false;
    }
    return true;
}

bool operator!=(const Block &block_a, const Block &block_b) {
    return !(block_a == block_b);
}

void Block::circularShift(unsigned char row, bool left) {
    unsigned char moved_byte_value;

    //Shifting bytes to the left
    if(left){
        moved_byte_value = this->bytes[row * 4].getValue();
        for (int i = 0; i < 3; ++i) {
            this->bytes[row * 4 + i].setValue(this->bytes[(row * 4) + (i + 1)].getValue());
        }
        this->bytes[row * 4 + 3].setValue(moved_byte_value);
    }
    //Shifting bytes to the right
    else{
        moved_byte_value = this->bytes[(row * 4)+3].getValue();
        for (int i = 3; i >= 0; --i) {
            this->bytes[row * 4 + i].setValue(this->bytes[(row * 4) + (i - 1)].getValue());
        }
        this->bytes[row * 4].setValue(moved_byte_value);
    }
}

unsigned char Block::getByte(unsigned char byte_index) {
    return this->bytes[byte_index].getValue();
}

Block* Block::shiftRows(bool inverse) {
    //Depending if argument is true or false shift "rows"
    circularShift(1, !inverse);

    circularShift(2, !inverse);
    circularShift(2, !inverse);

    circularShift(3, !inverse);
    circularShift(3, !inverse);
    circularShift(3, !inverse);

    return this;
}

Block* Block::substituteBytes(bool inverse) {
    for (auto & byte : this->bytes) {
        byte.substituteByte(inverse);
    }
    return this;
}

void Block::mixColumn(unsigned char column, bool inverse) {

    Byte* current_column[4];
    Byte new_column[4];

    //Get the column bytes in an array
    for (int i = 0; i < 4; ++i) {
        current_column[i] = &this->bytes[i * 4 + column];
    }

    //Gather the new column bytes without replacing the original
    if(!inverse){
        //FIPS 197 5.1.3 (5.6)
        new_column[0] =
                Byte(0x02) * current_column[0] xor
                Byte(0x03) * current_column[1] xor
                current_column[2] xor
                current_column[3];
        new_column[1] =
                current_column[0] xor
                (Byte(0x02) * current_column[1]) xor
                (Byte(0x03) * current_column[2]) xor
                current_column[3];
        new_column[2] =
                *current_column[0] xor
                *current_column[1] xor
                (Byte(0x02) * current_column[2]) xor
                (Byte(0x03) * current_column[3]);
        new_column[3] =
                (Byte(0x03) * current_column[0]) xor
                current_column[1] xor
                current_column[2] xor
                (Byte(0x02) * current_column[3]);
    }
    else{
        //FIPS 197 5.3.3 (5.10)
        new_column[0] =
                Byte(0x0e) * current_column[0] xor
                Byte(0x0b) * current_column[1] xor
                Byte(0x0d) * current_column[2] xor
                Byte(0x09) * current_column[3];
        new_column[1] =
                Byte(0x09) * current_column[0] xor
                Byte(0x0e) * current_column[1] xor
                Byte(0x0b) * current_column[2] xor
                Byte(0x0d) * current_column[3];
        new_column[2] =
                Byte(0x0d) * current_column[0] xor
                Byte(0x09) * current_column[1] xor
                Byte(0x0e) * current_column[2] xor
                Byte(0x0b) * current_column[3];
        new_column[3] =
                Byte(0x0b) * current_column[0] xor
                Byte(0x0d) * current_column[1] xor
                Byte(0x09) * current_column[2] xor
                Byte(0x0e) * current_column[3];
    }

    //Overwrite original byte values with mixed column ones
    for (int i = 0; i < 4; ++i) {
        this->bytes[i * 4 + column] = new_column[i];
    }
}

Block* Block::mixColumns(bool inverse) {

    //Mix each column
    for (int i = 0; i < 4; ++i) mixColumn(i,inverse);
    return this;

}
