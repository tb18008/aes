//
// Created by User on 01.11.2021..
//

#include "aes.h"

AES::AES() {

}

void AES::encryptBlock(const unsigned char *plaintext, const unsigned char *key, unsigned char (&ciphertext_result)[16]) {
    Block plaintext_block = Block(plaintext);
    Block ciphertext_block = plaintext_block;

    //Perform encryption

    this->writeOutBlock(ciphertext_block,ciphertext_result);
}

void AES::decryptBlock(const unsigned char *ciphertext, const unsigned char *key, unsigned char (&plaintext_result)[16]) {
    Block ciphertext_block = Block(ciphertext);
    Block plaintext_block = ciphertext_block;

    //Perform decryption

    this->writeOutBlock(plaintext_block,plaintext_result);
}

void AES::writeOutBlock(Block block, unsigned char (&array)[16]) {
    for (int i = 0; i < 16; ++i) array[i] = block.getByte(i);
}
